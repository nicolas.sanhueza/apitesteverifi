<?php

return [
    'url' => env('EVERIFI_URL'),
    'apiToken' => env('EVERIFI_API_TOKEN')
];
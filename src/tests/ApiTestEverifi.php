<?php

use NicolasSignalis\Apitesteverifi\Services\ApiTestEverifiServices;
use Illuminate\Support\Str;

beforeEach(function () {
    //Generate a random email
    $randitEmail = 'test.' . Str::random(3) . '@example.com';
    //Generate a random phone number
    $randitPhoneNumber = '+569' . rand(10000000, 99999999);
    $data = [
        'contact' => [
            'email' => $randitEmail,
            'phone_number' => $randitPhoneNumber
        ]
    ];
    $this->everifiAPI = app(ApiTestEverifiServices::class)->create($data);
});

it('Create EverifiAPI correctly', function ()
{
    $response = $this->everifiAPI;
    $this->assertIsArray($response);
    expect($response)
        ->toBeArray()
        ->toHaveKeys(['request_id', 'verification_id', 'status', 'status_reason']);
});

it('Create Endpoint EverifiAPI with incorrect email', function ()
{
    $everifi = app(ApiTestEverifiServices::class);
    $emailList = ['', 'test.', '@example.com', 'test@', 'test.@example', 'test@example.'];
    //Choose a random email in the list 
    $randitEmailList = $emailList[array_rand($emailList)];
    //Generate a random phone number
    $randitPhoneNumber = '+569' . rand(10000000, 99999999);
    $data = [
        'contact' => [
            'email' => $randitEmailList,
            'phone_number' => $randitPhoneNumber
        ]
    ];
    $response = $everifi->create($data);
    $this->assertIsArray($response);
    $this->assertEquals([
        'success' => false,
        'message' => 'Body validation errors',
        'data' => [
            'contact.email' => ['validation.email']
        ]
    ], $response);
});

it('Create Endpoint EverifiAPI with incorrect phone_number', function()
{
    $everifi = app(ApiTestEverifiServices::class);
    //Generate a random email 
    $randitEmail = 'test.' . Str::random(3) . '@example.com';
    $numberPhoneList = ['+5692888898383898828', '+1', '+569'];
    //Choose a random phone number in the list  
    $randitnumberPhoneList = $numberPhoneList[array_rand($numberPhoneList)];
    $data = [
        'contact' => [
            'email' => $randitEmail,
            'phone_number' => $randitnumberPhoneList
        ]
    ];
    $response = $everifi->create($data);
    $this->assertIsArray($response);
    $this->assertEquals([
        'success' => false,
        'message' => 'Body validation errors',
        'data' => [
            'contact.phone_number' => ['validation.phone_number']
        ]
    ], $response);
});

it('Create Endpoint EverifiAPI with email and phone_number have null valor', function()
{
    $everifi = app(ApiTestEverifiServices::class);
    $email = '';
    $phoneNumber = '';
    $data = [
        'contact' => [
            'email' => $email,
            'phone_number' => $phoneNumber
        ]
    ];
    $response = $everifi->create($data);
    $this->assertIsArray($response);
    $this->assertEquals([
        'success' => false,
        'message' => 'Body validation errors',
        'data' => [
            'contact.email' => ['Contact email is required'],
            'contact.phone_number' => ['Contact phone is required']
        ]
    ], $response);
});

it('Verification Endpoint EverifiAPI correctly', function () 
{
    $everifi = app(ApiTestEverifiServices::class);
    $createArray = $this->everifiAPI;
    $requestId = $createArray['verification_id'];
    $verification = $everifi->verification($requestId);
    $this->assertIsArray($verification);
    $this->assertEquals([
        'request_id' => $requestId,
        'verification_id' => $requestId,
        'status' => 'pending',
        'status_reason' => 'Contact has not opened or clicked on verification email',
        'verified_email' => $verification['verified_email'],
        'verified_phone_number' => $verification['verified_phone_number']
    ], $verification);
});

it('Verification Endpoint EverifiAPI with incorrect data or verification_id is not exist', function () 
{
    $everifi = app(ApiTestEverifiServices::class);
    $verificationId = ['98f470db', '1b0d7104-b383-11ed-ac9b-00155da06038','98f470db-1298-4b37-95d7-b7ef76000a1'];
    $randVerificationId = $verificationId[array_rand($verificationId)];
    $verification = $everifi->verification($randVerificationId);
    $this->assertIsArray($verification);
    $this->assertEquals([
        'message' => 'The verification id provided does not exist!'
    ], $verification);
});
 
it('Verification Endpoint EverifiAPI with null data verification_id', function()
{
    $everifi = app(ApiTestEverifiServices::class);
    $verificationId = '';
    $verification = $everifi->verification($verificationId);
    $this->assertIsArray($verification);
    $this->assertEquals([
        'message' => 'The verification id provided does not exist!'
    ], $verification);
});
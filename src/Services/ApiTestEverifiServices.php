<?php

namespace NicolasSignalis\Apitesteverifi\Services;

use Illuminate\Support\Facades\Http;

class ApiTestEverifiServices
{
    protected string $apiUrl;
    protected string $apiToken;

    public function __construct(string $apiUrl, string $apiToken)
    {
        $this->apiUrl = $apiUrl;
        $this->apiToken = $apiToken;
    }

    public function create($data)
    {
        $servicePath = 'verification/contact/create';
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $this->apiToken,
            'Content-Type' => 'application/json',
        ])->post($this->apiUrl . $servicePath, $data);
        $arrayRequest = $response->json();
        return $arrayRequest;

    }

    public function verification($id)
    {
        $servicePath = 'verification/contact/' . $id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $this->apiToken,
            'Content-Type' => 'application/json',
        ])->get($this->apiUrl . $servicePath);
        $arrayRequest = $response->json();
        return $arrayRequest;
    }
}
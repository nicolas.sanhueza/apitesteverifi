<?php

namespace NicolasSignalis\Apitesteverifi\Providers;

use Illuminate\Support\ServiceProvider;
use NicolasSignalis\Apitesteverifi\Services\ApiTestEverifiServices;

class ApiTestEverifiProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/ApiTestEverifiConfig.php', 'services.Everifi');

        $this->app->singleton(ApiTestEverifiServices::class, function($app)
        {
            return new ApiTestEverifiServices(
                apiUrl: config('services.Everifi.url'),
                apiToken: config('services.Everifi.apiToken'),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}